<?php

// allow cross orging (request from react can not be executed without this)
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

// DB credentials
$localhost = "localhost";
$username = "root";
$password = "";
$database_name = "POS";

// connect to DB
$mysqli = new mysqli($localhost, $username, $password, $database_name);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

// get request (R) method - one of CRUD
$qry = "SELECT * FROM items";
$results = mysqli_query($mysqli, $qry) OR die ("The Query Failed!");
$count = mysqli_num_rows($results);

if ($count > 0){
    $row = mysqli_fetch_all($results, MYSQLI_ASSOC);
    echo json_encode($row);
}
else{
    echo json_encode(array("message" => "No Product Found", "status" => false));
}

$mysqli->close();
?>