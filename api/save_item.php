<?php

// allow cross orging (request from react can not be executed without this)
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header("HTTP/1.1 200 OK");
die();
}

// DB credentials
$localhost = "localhost";
$username = "root";
$password = "";
$database_name = "POS";

// connect to DB
$mysqli = new mysqli($localhost, $username, $password, $database_name);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

// get data from react - request (R) method - one of CRUD
$requestBody = file_get_contents('php://input');
$id = json_decode($requestBody, true)['id'];
$name = json_decode($requestBody, true)['name'];
$cost = json_decode($requestBody, true)['cost'];
$price = json_decode($requestBody, true)['price'];
$category = json_decode($requestBody, true)['category'];

$example_name = "CCC";
$qry= "UPDATE `items` SET `name` = '{$name}', `category` = '{$category}', `price` = '{$price}', `cost` = '{$cost}', `quantity` = '10' WHERE `items`.`id` = '{$id}'";
$results = mysqli_query($mysqli, $qry) OR die ("The Query Failed!");

$mysqli->close();
?>