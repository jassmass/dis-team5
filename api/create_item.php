<?php

// allow cross orging (request from react can not be executed without this)
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header("HTTP/1.1 200 OK");
die();
}

// DB credentials
$localhost = "localhost";
$username = "root";
$password = "";
$database_name = "POS";

// connect to DB
$mysqli = new mysqli($localhost, $username, $password, $database_name);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

// get request (R) method - one of CRUD
$entityBody = file_get_contents('php://input');
$itemName = json_decode($entityBody, true)['item_name'];
$category = json_decode($entityBody, true)['category'];
$price = json_decode($entityBody, true)['price'];
$cost = json_decode($entityBody, true)['cost'];
echo $entityBody;
$qry = "INSERT INTO `items` (`name`, `category`, `price`, `cost`, `quantity`) VALUES ('{$itemName}', '{$category}', '{$price}', '{$cost}', '20');";
$results = mysqli_query($mysqli, $qry) OR die ("The Query Failed!");

$mysqli->close();
?>
