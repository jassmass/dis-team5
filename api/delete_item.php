<?php

// allow cross orging (request from react can not be executed without this)
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header("HTTP/1.1 200 OK");
die();
}

// DB credentials
$localhost = "localhost";
$username = "root";
$password = "";
$database_name = "POS";

// connect to DB
$mysqli = new mysqli($localhost, $username, $password, $database_name);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

// get request (R) method - one of CRUD
$entityBody = file_get_contents('php://input');
$id = json_decode($entityBody, true)['id'];
$qry = "DELETE FROM items WHERE ID = $id";
$results = mysqli_query($mysqli, $qry) OR die ("The Query Failed!");
//$count = mysqli_num_rows($results);

if ($results){
//    $row = mysqli_fetch_all($results, MYSQLI_ASSOC);
    echo json_encode($results);
}
else{
    echo json_encode(array("message" => "No Product Found", "status" => false));
}

$mysqli->close();
?>
