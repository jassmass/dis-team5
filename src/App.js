import './App.css';
import Login from './components/Login';
import ResetPassword from './components/ResetPassword';
import Dashboard from './components/Dashboard';
import ItemsPage from './components/ItemsPage';
import UserPage from './components/UserPage';
import CustomerPage from './components/CustomerPage';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@emotion/react';
import { Routes, Route, BrowserRouter } from "react-router-dom";
import NewItem from './components/NewItem';
import EditItem from './components/EditItem';
import PermissionsPage from './components/PermissionsPage';

function App() {
  const fontPoppins = "'Poppins'"
  const theme = createTheme({
    typography: {
      fontFamily: fontPoppins,
      h6:{fontSize:18,fontWeight:100}
    }
  })

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/resetPassword" element={<ResetPassword />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/items" element={<ItemsPage />} />
          <Route path="/users" element={<UserPage />} />
          <Route path="/customers" element={<CustomerPage />} />
          <Route path="/items-add" element={<NewItem />} />
          <Route path="/items-edit" element={<EditItem />} />
          <Route path="/permissions" element={<PermissionsPage />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;