import TextField from "@mui/material/TextField";
import { Autocomplete } from "@mui/material";
import "./DashboardHeader.css";
function DashboardHeader() {

  const searchOptions = [
    'Lasagna',
    'Pasta',
    'Pasta sauce',
    'Polenta',
    'Wine'
  ]

  return (
    <div className="header-container">
      <div className="header-left">
        <img className="header-logo-icon" src="logo.svg" alt="Logo" />
      </div>
      <div className="header-right">
        <div className="header-search-and-filter">
          <Autocomplete
            freeSolo
            className="header-search"
            id="dashboard-header-search"
            disableClearable
            options={searchOptions}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Search..."
                InputProps={{
                  ...params.InputProps,
                  type: "search",
                }}
              />
            )}
          />

          <Autocomplete
            id="dashboard-header-filter"
            className="header-filter"
            options={[]}
            sx={{ width: 300 }}
            renderInput={(params) => (
              <TextField {...params} label="Filter" />
            )}
          />
        </div>
        <div className="header-right-toolbar">
          <img src="flag.svg" alt="Logo" />
          <img src="notification.svg" alt="Logo" />
          <img src="user_avatar.svg" alt="Logo" />
        </div>
      </div>
    </div>
  );
}
export default DashboardHeader;
