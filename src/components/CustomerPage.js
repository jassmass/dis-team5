import React, { useState } from "react";
import "./CustomerPage.css";
import DashboardHeader from "./DashboardHeader";
import SideBarNavigation from "./SideBarNavigation";
import Table from "./Table";
import { items } from "./customers";
import { CssBaseline } from "@mui/material";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";

function CustomerPage() {
  const navigate = useNavigate();
  // const [isEditButtonVisible, setIsEditButtonVisisble] = useState(false);
  const columns = React.useMemo(
    () => [
      {
        Header: "List of customers",
        Footer: "Displaying 1-10 of 200",
        columns: [
          {
            Header: "NAME",
            accessor: "name",
            Cell: ({ row }) => {
              return (
                <div className="name-container">
                  <img height={34} src={row.original?.img} />
                  <div className="item-name-id">
                    <div className="item-name">{row.original?.name}</div>
                    <div className="item-id">{`ID:${row.original?.ID}`}</div>
                  </div>
                </div>
              );
            },
          },
          {
            Header: "CATEGORY",
            accessor: "category",
          },
          {
            Header: "ACTIONS",
            accessor: "actions",
            Cell: ({ row }) => {
              return (
                <div
                  className="action-container"
                >
                  <img src={row.original?.action} onClick={() => navigate('/items-edit')}/>
                  {/* {isEditButtonVisible && (
                    <Button
                      onClick={redirectToNewItem}
                      className="edit-item-button"
                      variant="contained"
                      style={{
                        fontSize: 16,
                        fontWeight: 700,
                        backgroundColor: "#1CBE20",
                      }}
                    >
                      Edit
                    </Button>
                  )} */}
                </div>
              );
            },
          },
        ],
      },
    ],
    []
  );

  const redirectToNewItem = () => {
    navigate("/items-add");
  };

  return (
    <>
      <DashboardHeader />
      <SideBarNavigation />
      <div className="items-page-container">
        <div className="items-page-upper-part">
          <div className="item-page-title-and-description">
            <div className="page-title">{"Customers"}</div>
            <div className="page-description">
              {"Here you can add customers"}
            </div>
          </div>
          <Button
            onClick={redirectToNewItem}
            className="add-new-item-button"
            variant="contained"
            style={{
              fontSize: 16,
              fontWeight: 700,
              backgroundColor: "#1CBE20",
            }}
          >
            ADD NEW CUSTOMER
          </Button>
        </div>
        <CssBaseline />
        <Table columns={columns} data={items} />
      </div>
    </>
  );
}
export default CustomerPage;
