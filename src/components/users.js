export const items = [
  {
    name: "Manager",
    category: "Management",
    img: "p_avatar.svg",
    ID: "142305",
    action: "action_icon.svg"
  },
  {
    name: "Cashier",
    category: "Employees",
    img: "b_avatar.svg",
    ID: "140650",
    action: "action_icon.svg"
  },
  {
    name: "Cashier",
    category: "Employees",
    img: "b_avatar.svg",
    ID: "140651",
    action: "action_icon.svg"
  },
  {
    name: "Cashier",
    category: "Employees",
    img: "b_avatar.svg",
    ID: "140652",
    action: "action_icon.svg"
  }
];
