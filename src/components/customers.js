export const items = [
  {
    name: "Taylor",
    category: "VIP",
    img: "p_avatar.svg",
    ID: "142305",
    action: "action_icon.svg"
  },
  {
    name: "Tony",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120222",
    action: "action_icon.svg"
  },
  {
    name: "Jack",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120223",
    action: "action_icon.svg"
  },
  {
    name: "Jacky",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120224",
    action: "action_icon.svg"
  },
  {
    name: "Carol",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120225",
    action: "action_icon.svg"
  },
  {
    name: "Paul",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120226",
    action: "action_icon.svg"
  },
  {
    name: "Elvis",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120227",
    action: "action_icon.svg"
  },
  {
    name: "Jasmine",
    category: "Customer",
    img: "b_avatar.svg",
    ID: "120228",
    action: "action_icon.svg"
  }
];
