import "./Dashboard.css";
import DashboardHeader from "./DashboardHeader";
import SideBarNavigation from "./SideBarNavigation";

function Dashboard() {
  return (
    <>
      <DashboardHeader />
      <SideBarNavigation />
    </>
  );
}
export default Dashboard;
