import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import './ResetPassword.css';
import { useNavigate } from 'react-router-dom';

function ResetPassword (){ 
    const navigate = useNavigate();

    const redirectToLogin = () => {
      navigate('/');
    }
    return (
        <div className='reset-password-page-container'>
            <div>
                <div className='heading-reset-password-container'>
                    <Typography 
                        variant="h5" 
                        style={{fontWeight:600}}>
                        Forgot your Password
                    </Typography>

                    <Typography 
                        variant="h6" 
                        style={{fontWeight:400,color:'#575A7B'}}>
                        Please enter you email address you used when you join and we will send you link to reset you password.
                    </Typography>
                </div>

                <div className='input-fileds-reset-password'>
                    <TextField 
                        label="Email address"
                        color="success" 
                        inputProps={{style: {fontSize: 14}}} 
                        InputLabelProps={{style: {fontSize: 14}}}
                    />  
                   
                </div>

                <div className='reset-password-button-container'>
                    <Button 
                        className='reset-password-button'
                        variant="contained"
                        style={{fontSize:16,fontWeight:700,backgroundColor:'#1CBE20'}}
                    >
                        Request recovery link
                    </Button>
                </div>

                <div className='pasword-and-reset-container'>
                    <Typography 
                        variant="body2"
                        style={{fontWeight:400,color:'#575A7B'}}
                    >
                        Remembered your password?
                    </Typography>

                    <Button 
                        variant="text"
                        onClick={redirectToLogin}
                        style={{fontWeight:400,color:'#1CBE20', padding:0, marginLeft:16}}
                    >
                        Back to login
                    </Button>
                
                </div>

            </div>
            
            
        </div>

      );
} 
export default ResetPassword
