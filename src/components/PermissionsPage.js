import React, { useState } from "react";
import "./Permissions.css";
import "./PermissionsList.js";
import DashboardHeader from "./DashboardHeader";
import SideBarNavigation from "./SideBarNavigation";
import { CssBaseline } from "@mui/material";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import PermissionsList from "./PermissionsList.js";

function PermissionsPage() {
  const navigate = useNavigate();
  // const [isEditButtonVisible, setIsEditButtonVisisble] = useState(false);
  
  const redirectToNewItem = () => {
    navigate("/items-add");
  };

  return (
    <>
      <DashboardHeader />
      <SideBarNavigation />
      <div className="permissions-page-container">
        <div className="permissions-page-upper-part">
          <div className="permissions-page-title-and-description">
            <div className="page-title">{"Permissions"}</div>
            <div className="page-description">
              {"Set Permissions for the Administrator"}
            </div>
          </div>
          <Button
            className="add-new-permission-button"
            variant="contained"
            style={{
              fontSize: 16,
              fontWeight: 700,
              backgroundColor: "#1CBE20",
            }}
          >
            NEW PERMISSION
          </Button>
        </div>
        <CssBaseline />
        <PermissionsList />
      </div>
    </>
  );
}
export default PermissionsPage;
