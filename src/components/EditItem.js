import "./EditItem.css";
import SideBarNavigation from "./SideBarNavigation";
import DashboardHeader from "./DashboardHeader";
import {
  Autocomplete,
  Button,
  Checkbox,
  FormControlLabel,
  Stack,
  TextField,
} from "@mui/material";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";


function EditItem() {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();
  const [name, setName]=useState('');
  const [price, setPrice]=useState('');
  const [cost, setCost]=useState('');
  const [category, setCategory]=useState('');
  const id = searchParams.get('id');

  const onNameChange = (event) => {
    setName (event.target.value)
  }
  const onCategoryChange = (event) => {
    setCategory (event.target.value)
  }
  const onPriceChange = (event) => {
    setPrice (event.target.value)
  }
  const onCostChange = (event) => {
    setCost (event.target.value)
  }
  const onSaveButtonClick = () => {
    axios.put("http://localhost:8080/api/save_item.php",{
      "id":id,
      "name":name,
      "category":category,
      "price":price,
      "cost":cost,
    })
    navigate("/items")
  }
  const deleteItem = (id) => {
    axios.delete("http://localhost:8080/api/delete_item.php",{"data":{"id":id}})
    navigate("/items")
  };

  //call server/api
  useEffect(() => {
    console.log('id sa stranice', id);
    axios.post("http://localhost:8080/api/read_item.php",{"id":id})
    .then(data => {
      console.log ('podaci iz pm',data)
      setName(data.data[0]?.name);
      setPrice(data.data[0]?.price);
      setCost(data.data[0]?.cost);
      setCategory(data.data[0]?.category)
    });
  },[])

  return (
    <div>
      <DashboardHeader />
      <SideBarNavigation />
      <div className="edit-item-main-container">
        <div>
          <div className="item-page-title-and-description">
            <div className="page-title">{"Edit Item"}</div>
            <div className="page-description">
              {"This information will be displayed on the front page"}
            </div>
          </div>
          <div className="item-name-category">
            <TextField
              label="Item name"
              className="edit-item-name"
              color="success"
              value={name}
              onChange={onNameChange}
            />
             <TextField
              label="Category"
              className="edit-item-name"
              color="success"
              value={category}
              onChange={onCategoryChange}
              />
          </div>
          <div className="edit-item-price-container">
            <div className="edit-item-price-cost">
              <TextField
                label="Price"
                className="edit-item-price"
                color="success"
                value={price}
                onChange={onPriceChange}
              />
              <TextField
                className="edit-item-cost"
                label="Cost"
                color="success"
                value={cost}
                onChange={onCostChange}
              />
            </div>

            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked
                  color="success"
                  className="edit-item-checkbox"
                />
              }
              label="This item is available for sale"
              defaultChecked
            />
          </div>
          <div className="edit-item-actions">
            <Stack direction="row" spacing={2}>
              <Button
                variant="contained"
                startIcon={<img src="icon_close.svg" />}
                style={{
                  height: 52,
                  fontWeight: 400,
                  color: "#EB2E50",
                  width: "149px",
                  backgroundColor: "#FFFFFF",
                }}
                // onClick={() => navigate("/items")}
                onClick={() => deleteItem (id)}
              >
                DELETE
              </Button>
              <Button
                variant="contained"
                startIcon={<img src="icon_save.svg" />}
                style={{
                  fontWeight: 400,
                  height: 52,
                  width: "156px",
                  color: "#FFFFFF",
                  backgroundColor: "#1CBE20",
                }}
              
                onClick = {onSaveButtonClick}
              >
                SAVE
              </Button>
            </Stack>
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditItem;
