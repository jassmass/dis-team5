import "./NewItem.css";
import SideBarNavigation from "./SideBarNavigation";
import DashboardHeader from "./DashboardHeader";
import {
  Autocomplete,
  Button,
  Checkbox,
  FormControlLabel,
  Stack,
  TextField,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useState } from 'react';

function NewItem() {
  const navigate = useNavigate();
  const searchOptions = ["Main Dishes", "Extra", "Drink", "No category"];
  const [itemName, setItemName] = useState('');
  const [category, setCategory] = useState('');
  const [price, setPrice] = useState('');
  const [cost, setCost] = useState('');

  const onItemNameChanged = (event) => {
    setItemName(event.target.value);
  }
  const onCategoryChanged = (event) => {
    setCategory(event.target.value);
  }
  const onPriceChanged = (event) => {
    setPrice(event.target.value);
  }
  const onCostChanged = (event) => {
    setCost(event.target.value);
  }

  const onItemSave = () => {
    console.log(itemName, category, price, cost);
    axios.post("http://localhost/api/create_item.php", {
      "item_name": itemName,
      "category": category,
      "price": price,
      "cost": cost,
    })
    .then(data => {
      navigate("/items")
    });
  }

  return (
    <div>
      <DashboardHeader />
      <SideBarNavigation />
      <div className="new-item-main-container">
        <div>
          <div className="item-page-title-and-description">
            <div className="page-title">{"New Item"}</div>
            <div className="page-description">
              {"This information will be displayed on the front page"}
            </div>
          </div>
          <div className="item-name-category">
            <TextField
              label="Item name"
              className="new-item-name"
              color="success"
              value={itemName.value}
              onChange={onItemNameChanged}
            />
            <TextField
              label="Category"
              className="new-item-category"
              color="success"
              value={category.value}
              onChange={onCategoryChanged}
            />
          </div>
          <div className="new-item-price-container">
            <div className="new-item-price-cost">
              <TextField
                label="Price"
                className="new-item-price"
                color="success"
                value={price.value}
                onChange={onPriceChanged}
              />
              <TextField
                className="new-item-cost"
                label="Cost"
                color="success"
                value={cost.value}
                onChange={onCostChanged}
              />
            </div>

            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked
                  color="success"
                  className="new-item-checkbox"
                />
              }
              label="This item is available for sale"
              defaultChecked
            />
          </div>
          <div className="new-item-actions">
            <Stack direction="row" spacing={2}>
              <Button
                variant="contained"
                startIcon={<img src="icon_close.svg" />}
                style={{
                  height: 52,
                  fontWeight: 400,
                  color: "#EB2E50",
                  width: "149px",
                  backgroundColor: "#FFFFFF",
                }}
                onClick={() => navigate("/items")}
              >
                CANCEL
              </Button>
              <Button
                variant="contained"
                startIcon={<img src="icon_save.svg" />}
                style={{
                  fontWeight: 400,
                  height: 52,
                  width: "156px",
                  color: "#FFFFFF",
                  backgroundColor: "#1CBE20",
                }}
                onClick={onItemSave}
              >
                SAVE
              </Button>
            </Stack>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewItem;
