import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import './Login.css';
import  { GoogleLogin } from 'react-google-login';
import { useNavigate } from 'react-router-dom';


function Login (){ 
    const navigate = useNavigate();

    const redirectToResetPassword = () => {
      navigate('/resetPassword');
    }
    const redirectToDashboard = () => {
        navigate('/dashboard');
      }

    return (
        <div className='login-page-container'>
            <div>
                <div className='heading-login-container'>
                    <Typography 
                        variant="h5" 
             
             style={{fontWeight:600}}>
                        Sign in into you POS account!
                    </Typography>

                    <Typography 
                        variant="h6" 
                        style={{fontWeight:400,color:'#575A7B'}}>
                        Use your valid username and password.
                    </Typography>
                </div>

                <div className='input-fileds-login'>
                    <TextField 
                        label="Username or email"
                        color="success" 
                        inputProps={{style: {fontSize: 14}}} 
                        InputLabelProps={{style: {fontSize: 14}}}
                    />  
                    <TextField 
                        label="Password"
                        color="success" 
                        inputProps={{style: {fontSize: 14}}} 
                        InputLabelProps={{style: {fontSize: 14}}}
                    />  
                </div>

                <div className='login-button-container'>
                    <Button
                        onClick={redirectToDashboard} 
                        className='login-button'
                        variant="contained"
                        style={{fontSize:16,fontWeight:700,backgroundColor:'#1CBE20'}}
                    >
                        LOGIN
                    </Button>
                </div>

                <div className='pasword-and-reset-container'>
                    <Typography 
                        variant="body2"
                        style={{fontWeight:400,color:'#575A7B'}}
                    >
                        Forgot password?
                    </Typography>

                    <Button 
                        variant="text"
                        onClick={redirectToResetPassword}
                        style={{fontWeight:400,color:'#1CBE20', padding:0}}
                    >
                        RESET
                    </Button>
                
                </div>
                {/* <div className='google-login-container' >
                    <GoogleLogin
                        // clientId={clientId}
                        buttonText="Sign in with Google"
                        className='google-login-button'
                        // onSuccess={onSuccess}
                        // onFailure={onFailure}
                        // cookiePolicy={'single_host_origin'}
                        style={{  width: '496px' }}
                    />
                </div> */}

            </div>
            
            
        </div>

      );
} 
export default Login
