import React, { useState, useEffect } from "react";
import "./ItemsPage.css";
import DashboardHeader from "./DashboardHeader";
import SideBarNavigation from "./SideBarNavigation";
import Table from "./Table";
import { CssBaseline } from "@mui/material";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import EditItem from "./EditItem";
import axios from "axios";

function ItemsPage() {
  const navigate = useNavigate();
  const [items, setItems] = useState([]);
  const editItem = (id)=> {
    navigate(`/items-edit?id=${id}`);
  }


  useEffect(() => {
    axios.get("http://localhost:8080/api/read_items.php")
    .then(data => {
      setItems(data.data);
    });
  },[])

  const columns = React.useMemo(
    () => [
      {
        Header: "List of items",
        Footer: "Displaying 1-10 of 200",
        columns: [
          {
            Header: "NAME",
            accessor: "item_name",
            Cell: ({ row }) => {
              return (
                <div className="name-container">
                  <img height={34} src={row.original?.img} />
                  <div className="item-name-id">
                    <div className="item-name">{row.original?.name}</div>
                    <div className="item-id">{`ID:${row.original?.id}`}</div>
                  </div>
                </div>
              );
            },
          },
          {
            Header: "PRICE",
            accessor: "price",
          },
          {
            Header: "COST",
            accessor: "cost",
          },
          {
            Header: "QUANTITY",
            accessor: "quantity",
          },
          {
            Header: "ACTIONS",
            accessor: "actions",
            Cell: ({ row,data }) => {
              return (
                <div className="action-container">
                  <Button
                    onClick={() => editItem(row.original.id)}
                    className="edit-item-button"
                    variant="contained"
                    style={{
                      fontSize: 16,
                      fontWeight: 700,
                      backgroundColor: "#1CBE20",
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                      onClick={() => deleteItem (row.original.id,data)} 
                      className="edit-item-button"
                      variant="text"
                    >
                      Delete
                    </Button>
                </div>
              );
            },
          },
        ],
      },
    ],
    []
  );

  const redirectToNewItem = () => {
    navigate("/items-add");
  };

  const deleteItem = (id,data) => {
    console.log(id)
    axios.delete("http://localhost:8080/api/delete_item.php",{"data":{"id":id}})
    const restItems = data.filter (item => item.id !== id);
    setItems (restItems)
  };
  return (
    <>
      <DashboardHeader />
      <SideBarNavigation />
      <div className="items-page-container">
        <div className="items-page-upper-part">
          <div className="item-page-title-and-description">
            <div className="page-title">{"Items"}</div>
            <div className="page-description">
              {"Here you can manage items"}
            </div>
          </div>
          <Button
            onClick={redirectToNewItem}
            className="add-new-item-button"
            variant="contained"
            style={{
              fontSize: 16,
              fontWeight: 700,
              backgroundColor: "#1CBE20",
            }}
          >
            ADD NEW ITEM
          </Button>
        </div>
        <CssBaseline />
        <Table columns={columns} data={items} />
      </div>
    </>
  );
}
export default ItemsPage;
