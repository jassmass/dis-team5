import * as React from "react";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import "./SideBarNavigation.css";
import { useNavigate } from "react-router-dom";

const drawerWidth = 331;

export default function SideBarNavigation() {
  const navigate = useNavigate();
  
  return (
    <Drawer
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        "& .MuiDrawer-paper": {
          marginTop: "91px",
          width: drawerWidth,
          paddingLeft: "50px",
          boxSizing: "border-box",
        },
      }}
      variant="permanent"
      anchor="left"
    >
      {/* <Toolbar /> */}
      <List>
        <ListItem className="section-header">
          <ListItemText primary={"DASHBOARD"} />
        </ListItem>
      </List>

      <List>
        <ListItem button key="dashboard" className="" onClick={() => navigate('/dashboard')}>
          <ListItemIcon>
            <img src="chart.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
        <ListItem button key="sales by item" className="">
          <ListItemIcon>
            <img src="sales-by-item.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Sales By Item" />
        </ListItem>
        <ListItem button key="sales by category" className="">
          <ListItemIcon>
            <img src="sales-by-category.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Sales By Category" />
        </ListItem>
        <ListItem button key="customers" className="" onClick={() => {navigate('/customers')}}>
          <ListItemIcon>
            <img src="customers.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Customers" />
        </ListItem>
        <ListItem button key="item-list" className="" onClick={() => {navigate('/items')}}>
          <ListItemIcon>
            <img src="item-list.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Item list" />
        </ListItem>
        <ListItem button key="categories" className="">
          <ListItemIcon>
            <img src="categories.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Categories" />
        </ListItem>
      </List>

      <ListItem className="section-header">
        <ListItemText primary={"USERS"} />
      </ListItem>

      <List>
        <ListItem button key="users" className="" onClick={() => {navigate('/users')}}>
          <ListItemIcon>
            <img src="users.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Users" />
        </ListItem>
        <ListItem button key="role" className="">
          <ListItemIcon>
            <img src="role.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Role" />
        </ListItem>
        <ListItem button key="permission" className="" onClick={() => {navigate('/permissions')}}>
          <ListItemIcon>
            <img src="permission.svg" alt="Chart" />
          </ListItemIcon>
          <ListItemText primary="Permission" />
        </ListItem>
      </List>
    </Drawer>
  );
}
