import React, { useState } from "react";
import "./Permissions.css";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';

export const permissions1 = [
  {
    ID: 1,
    primaryText: "Manage Accounts",
    secondaryText: "The user can manage accounts with this permission",
    isChecked: true
  },
  {
    ID: 2,
    primaryText: "Manage Roles",
    secondaryText: "The user can manage roles with this permission",
    isChecked: true
  },
  {
    ID: 3,
    primaryText: "Create User",
    secondaryText: "The user can create accounts with this permission",
    isChecked: true
  },
  {
    ID: 4,
    primaryText: "Edit User",
    secondaryText: "The user can edit users with this permission",
    isChecked: true
  },
  {
    ID: 5,
    primaryText: "Delete User",
    secondaryText: "The user can delete user with this permission",
    isChecked: true
  },
  {
    ID: 6,
    primaryText: "View User",
    secondaryText: "The user can view users with this permission",
    isChecked: true
  }
];

export const permissions2 = [
  {
    ID: 7,
    primaryText: "Manage Items",
    secondaryText: "The user can manage items with this permission",
    isChecked: true
  },
  {
    ID: 8,
    primaryText: "Manage Categories",
    secondaryText: "The user can manage categories with this permission",
    isChecked: false
  },
  {
    ID: 9,
    primaryText: "Create Receipts",
    secondaryText: "The user can create receipts with this permission",
    isChecked: false
  },
  {
    ID: 10,
    primaryText: "Print Receipts",
    secondaryText: "The user can print receipts with this permission",
    isChecked: true
  },
  {
    ID: 11,
    primaryText: "Generate Reports",
    secondaryText: "The user can generate receipts with this permission",
    isChecked: false
  },
  {
    ID: 12,
    primaryText: "Search Users",
    secondaryText: "The user can search users with this permission",
    isChecked: false
  }
];

export const permissions3 = [
  {
    ID: 13,
    primaryText: "Manage Customers",
    secondaryText: "The user can manage customers with this permission",
    isChecked: true
  },
  {
    ID: 14,
    primaryText: "Manage Access",
    secondaryText: "The user can manage customer access with this permission",
    isChecked: false
  },
  {
    ID: 15,
    primaryText: "Create Customer",
    secondaryText: "The user can create customer with this permission",
    isChecked: false
  },
  {
    ID: 16,
    primaryText: "Edit Customer",
    secondaryText: "The user can edit customer with this permission",
    isChecked: true
  },
  {
    ID: 17,
    primaryText: "Delete Customer",
    secondaryText: "The user can delete customer with this permission",
    isChecked: false
  },
  {
    ID: 18,
    primaryText: "View Customer",
    secondaryText: "The user can view customers with this permission",
    isChecked: false
  }
];

function PermissionsList() {
  const [checked, setChecked] = React.useState([0]);
  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  return (
    <div className="permissions-container">
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
      >
        <Grid item xs={4}>
    <List>
      {permissions1.map((permission) => {
        const labelId = `checkbox-list-label-${permission.ID}`;

        return (
          <ListItem
            key={permission.ID}
            disablePadding
          >
            <ListItemButton role={undefined} onClick={handleToggle(permission.ID)} dense>
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={permission.isChecked}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                  color="success"
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={permission.primaryText} secondary={permission.secondaryText} />
            </ListItemButton>
          </ListItem>
        );
      })}
    </List>
    </Grid>
    <Grid item xs={4}>
    <List>
    {permissions2.map((permission) => {
      const labelId = `checkbox-list-label-${permission.ID}`;

      return (
        <ListItem
          key={permission.ID}
          disablePadding
        >
          <ListItemButton role={undefined} onClick={handleToggle(permission.ID)} dense>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={permission.isChecked}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': labelId }}
                color="success"
                width={100}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={permission.primaryText} secondary={permission.secondaryText}/>
          </ListItemButton>
        </ListItem>
      );
    })}
  </List>
  </Grid>
  <Grid item xs={4}>
  <List width='100px'>
  {permissions3.map((permission) => {
    const labelId = `checkbox-list-label-${permission.ID}`;

    return (
      <ListItem
        key={permission.ID}
        disablePadding
      >
        <ListItemButton role={undefined} onClick={handleToggle(permission.ID)} dense>
          <ListItemIcon>
            <Checkbox
              edge="start"
              checked={permission.isChecked}
              tabIndex={-1}
              disableRipple
              inputProps={{ 'aria-labelledby': labelId }}
              color="success"
            />
          </ListItemIcon>
          <ListItemText id={labelId} primary={permission.primaryText} secondary={permission.secondaryText}/>
        </ListItemButton>
      </ListItem>
    );
  })}
</List>
</Grid>
</Grid>
</div>

  );
}
export default PermissionsList;
